import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegerTableListComponent } from './leger-table-list.component';

describe('LegerTableListComponent', () => {
  let component: LegerTableListComponent;
  let fixture: ComponentFixture<LegerTableListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegerTableListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegerTableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
