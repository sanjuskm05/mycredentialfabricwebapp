import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { CredentialDetailComponent } from './credential-detail/credential-detail.component';
import { ManageCredentialsComponent } from './manage-credentials/manage-credentials.component';
import { LegerTableListComponent } from './leger-table-list/leger-table-list.component';

const routes: Routes =[
    { path: 'credential-detail', component: CredentialDetailComponent },
    { path: 'manage-credentials', component: ManageCredentialsComponent },
    { path: 'leger-table-list', component: LegerTableListComponent},
    { path: '', redirectTo: 'leger-table-list', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
