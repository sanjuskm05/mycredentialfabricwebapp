import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { ExpansionPanelsModule } from 'ng2-expansion-panels';

import { AppComponent } from './app.component';
import { CredentialDetailComponent } from './credential-detail/credential-detail.component';
import { ManageCredentialsComponent } from './manage-credentials/manage-credentials.component';
import { LegerTableListComponent } from './leger-table-list/leger-table-list.component';

@NgModule({
  declarations: [
    AppComponent,
    CredentialDetailComponent,
    ManageCredentialsComponent,
    LegerTableListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    ExpansionPanelsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
